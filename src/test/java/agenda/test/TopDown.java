package agenda.test;

import agenda.exceptions.InvalidFormatException;
import agenda.model.base.Activity;
import agenda.model.base.Contact;
import agenda.model.repository.classes.RepositoryActivityFile;
import agenda.model.repository.classes.RepositoryContactFile;
import agenda.model.repository.interfaces.RepositoryActivity;
import agenda.model.repository.interfaces.RepositoryContact;
import org.junit.Before;
import org.junit.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class TopDown {


    private RepositoryActivity repAct;
    private RepositoryContact repCon;


    private Activity act;
    private Contact con;

    @Before
    public void setup() throws Exception {
        repCon = new RepositoryContactFile();
        repAct = new RepositoryActivityFile(repCon);

        for (Activity a : repAct.getActivities())
            repAct.removeActivity(a);
    }



    @Test
    public void testCaseA()
    {
        try {
            con = new Contact("name", "address123", "+407112257", "emaifwerwerwels@sderewrewew.com");
            repCon.addContact(con.getName(), con.getAddress(), con.getTelefon(), con.getEmail());
            for(Contact c : repCon.getContacts())
                if (c.equals(con))
                {
                    assertTrue(true);
                    break;
                }
        } catch (InvalidFormatException e) {
            assertTrue(false);
        }
    }


    @Test
    public void testCaseAB()
    {
        testCaseA();

        List<Contact> contacts= new LinkedList<Contact>();
        contacts.add(con);

        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        try {
            act = new Activity("name15",
                    df.parse("03/20/2013 12:00"),
                    df.parse("03/20/2013 13:00"),
                    contacts,
                    "Lunches break");
            repAct.addActivity(act);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        assertTrue(1 == act.getContacts().size());
    }

    @Test
    public void testCaseABC() {
        testCaseAB();

        List<Activity> result = repAct.activitiesByName("name15");
        assertTrue(result.size() == 1);
    }



}
