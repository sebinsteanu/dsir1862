package agenda.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import agenda.exceptions.InvalidFormatException;

import agenda.model.base.Contact;
import agenda.model.repository.classes.RepositoryContactMock;
import agenda.model.repository.interfaces.RepositoryContact;


public class AddContactTest {

	private Contact con;
	private RepositoryContact rep;
	
	@Before
	public void setUp(){
		rep = new RepositoryContactMock();
	}
	
	@Test
	public void testCasePhoneValid1()
	{
		try {
			con = new Contact("name", "address1", "+4071122334455", "email1@sdf.com");
            rep.addContact(con.getName(), con.getAddress(), con.getTelefon(), con.getEmail());
            for(Contact c : rep.getContacts())
                if (c.equals(con))
                {
                    assertTrue(false);
                    break;
                }
		} catch (InvalidFormatException e) {
			assertTrue(true);
		}
		//int n = rep.count();
		//assertTrue(n+1 == rep.count());
	}

	@Test
	public void testCase2()
	{
		try{
			rep.addContact("","","","");
		}
		catch(Exception e)
		{
			assertTrue(true);
		}
	}

	@Test
	public void testCasePhoneInvalid2()
	{
		for(Contact c : rep.getContacts())
			rep.removeContact(c);

		try {
			con = new Contact("name", "address1", "+07112233443223","email33@dsc.vox");
            rep.addContact(con.getName(), con.getAddress(), con.getTelefon(), con.getEmail());
		} catch (InvalidFormatException e) {
			assertTrue(true);
		}
		int n  = rep.count();
		if (n == 1)
			if (con.equals(rep.getContacts().get(0))) assertTrue(false);
			else assertTrue(true);
		else assertTrue(true);
	}


    @Test
    public void testCasePhoneValid2()
    {
        try {
            con = new Contact("name", "address1", "0711226733", "email1@sdf.com");
        } catch (InvalidFormatException e) {
            assertTrue(false);
        }
        //int n = rep.count();
        rep.addContact(con.getName(), con.getAddress(), con.getTelefon(), con.getEmail());
        for(Contact c : rep.getContacts())
            if (c.equals(con))
            {
                assertTrue(true);
                break;
            }
        //assertTrue(n+1 == rep.count());
    }


    @Test
    public void testCasePhoneInvalid()
    {
        try {
            con = new Contact("name ee", "address1", "0712", "email1@sdf.com");
        } catch (InvalidFormatException e) {
            assertTrue(true);
        }
        //int n = rep.count();
        if(con != null)
            rep.addContact(con.getName(), con.getAddress(), con.getTelefon(), con.getEmail());
        for(Contact c : rep.getContacts())
            if (c.equals(con))
            {
                assertTrue(false);
                break;
            }
        //assertTrue(n+1 == rep.count());
    }





    @Test
    public void testCaseAddressInvalid()
    {
        try {
            con = new Contact("name ee", "", "07123289", "email1@sdf.com");
        } catch (InvalidFormatException e) {
            assertTrue(true);
        }
        //int n = rep.count();
        if(con != null)
            rep.addContact(con.getName(), con.getAddress(), con.getTelefon(), con.getEmail());
        for(Contact c : rep.getContacts())
            if (c.equals(con))
            {
                assertTrue(false);
                break;
            }
        //assertTrue(n+1 == rep.count());
    }



    @Test
    public void testCaseNameInvalid()
    {
        try {
            con = new Contact("Nume 22", "address1", "07125543", "email1@sdf.com");
        } catch (InvalidFormatException e) {
            assertTrue(true);
        }
        //int n = rep.count();
        if(con != null)
            rep.addContact(con.getName(), con.getAddress(), con.getTelefon(), con.getEmail());
        for(Contact c : rep.getContacts())
            if (c.equals(con))
            {
                assertTrue(false);
                break;
            }
        //assertTrue(n+1 == rep.count());
    }


    @Test
    public void testCaseEmailInvalid()
    {
        try {
            con = new Contact("Nume", "address1", "0713452", "email1@sdf.c");
        } catch (InvalidFormatException e) {
            assertTrue(true);
        }
        //int n = rep.count();
        if(con != null)
            rep.addContact(con.getName(), con.getAddress(), con.getTelefon(), con.getEmail());
        for(Contact c : rep.getContacts())
            if (c.equals(con))
            {
                assertTrue(false);
                break;
            }
        //assertTrue(n+1 == rep.count());
    }


    @Test
    public void testCaseEmailInvalid2()
    {
        try {
            con = new Contact("Nume", "address1", "0713452", "email1@sdf.csdfsdf");
        } catch (InvalidFormatException e) {
            assertTrue(true);
        }
        if(con != null)
            rep.addContact(con.getName(), con.getAddress(), con.getTelefon(), con.getEmail());
        for(Contact c : rep.getContacts())
            if (c.equals(con))
            {
                assertTrue(false);
                break;
            }
        //assertTrue(n+1 == rep.count());
    }


    @Test
    public void testCaseEmailValid()
    {
        try {
            con = new Contact("name", "address1", "+40711225", "emaiwerwerwels@sderewrewew.com");
            rep.addContact(con.getName(), con.getAddress(), con.getTelefon(), con.getEmail());
            for(Contact c : rep.getContacts())
                if (c.equals(con))
                {
                    assertTrue(true);
                    break;
                }
        } catch (InvalidFormatException e) {
            assertTrue(false);
        }
        //int n = rep.count();
        //assertTrue(n+1 == rep.count());
    }


}
