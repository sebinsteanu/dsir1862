package agenda.model.repository.classes;

import agenda.exceptions.InvalidFormatException;
import agenda.model.base.Activity;
import agenda.model.base.Contact;
import agenda.model.repository.interfaces.RepositoryActivity;
import org.junit.Before;
import org.junit.Test;
import org.omg.CosNaming.NamingContextPackage.NotFound;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;

import static org.junit.Assert.*;

public class RepositoryActivityFileTest {

    private Activity activity;
    private RepositoryActivity rep;

    @Before
    public void setUp(){
        try {
            rep = new RepositoryActivityFile(new RepositoryContactFile());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    public void addActivity2() {

        activity = new Activity("activity_name22", new Date("12/12/1999"), new Date("11/11/2011"), new LinkedList<Contact>(), "description22");
        boolean added = rep.addActivity(activity);

        assertTrue(added);

        for(Activity a : rep.getActivities())
            if (a.equals(activity))
            {
                assertTrue(true);
                break;
            }

        assertTrue(true);
    }


    /***
     * a doua activitate incepe inaintea sfarsitului primei activitati
     */

    @Test
    public void addActivity3() {

        Calendar c = Calendar.getInstance();

        String dateS = "12/12/1999";
        String timeS = "12:48";

        String dateE = "12/12/1999";
        String timeE = "13:47";


        c.set(Integer.parseInt(dateS.split("/")[2]),
                Integer.parseInt(dateS.split("/")[0]) - 1,
                Integer.parseInt(dateS.split("/")[1]),
                Integer.parseInt(timeS.split(":")[0]),
                Integer.parseInt(timeS.split(":")[1]));

        Date start1 = c.getTime();
        c.add(1,-2);
        Date start2 = c.getTime();

        c.set(Integer.parseInt(dateE.split("/")[2]),
                Integer.parseInt(dateE.split("/")[0]) - 1,
                Integer.parseInt(dateE.split("/")[1]),
                Integer.parseInt(timeE.split(":")[0]),
                Integer.parseInt(timeE.split(":")[1]));

        Date end1 = c.getTime();
        c.add(1, 2);
        Date end2 = c.getTime();


        activity = new Activity("activity_name2332", start1, end1, new LinkedList<Contact>(), "description2332");
        boolean added1 = rep.addActivity(activity);

        activity = new Activity("activity_name2332", start2, end2, new LinkedList<Contact>(), "description2332");
        boolean added2 = rep.addActivity(activity);

        assertTrue(added1 && !added2);
    }



    /***
     * prima activitate incepe inaintea celei de-a doua activitati
     */

    @Test
    public void addActivity34() {

        Calendar c = Calendar.getInstance();

        String dateS = "12/12/2005";
        String timeS = "12:48";

        String dateE = "12/12/2005";
        String timeE = "13:47";


        c.set(Integer.parseInt(dateS.split("/")[2]),
                Integer.parseInt(dateS.split("/")[0]) - 1,
                Integer.parseInt(dateS.split("/")[1]),
                Integer.parseInt(timeS.split(":")[0]),
                Integer.parseInt(timeS.split(":")[1]));

        Date start2 = c.getTime();
        c.add(Calendar.DAY_OF_YEAR,-2);
        Date start1 = c.getTime();

        c.set(Integer.parseInt(dateE.split("/")[2]),
                Integer.parseInt(dateE.split("/")[0]) - 1,
                Integer.parseInt(dateE.split("/")[1]),
                Integer.parseInt(timeE.split(":")[0]),
                Integer.parseInt(timeE.split(":")[1]));

        Date end2 = c.getTime();
        c.add(Calendar.DAY_OF_YEAR, 2);
        Date end1 = c.getTime();

        activity = new Activity("activity_name2332", start2, end2, new LinkedList<Contact>(), "description2332");
        boolean added2 = rep.addActivity(activity);

        activity = new Activity("activity_name2332", start1, end1, new LinkedList<Contact>(), "description2332");
        boolean added1 = rep.addActivity(activity);


        assertTrue(!added1 && added2);
    }



}