package agenda.model.base;

import agenda.exceptions.InvalidFormatException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Contact {
	private String Name;
	private String Address;
	private String Telefon;
	private String Email;


	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
	public static final Pattern VALID_PHONE_REGEX = Pattern.compile("^[\\+0][\\d]{5,10}$", Pattern.CASE_INSENSITIVE);
	public static final Pattern VALID_NAME_REGEX = Pattern.compile("^([\\s]|[^0-9]|-){3,50}$", Pattern.CASE_INSENSITIVE);


	public Contact(){
		Name = "";
		Address = "";
		Telefon = "";
		Email = "";
	}

	public Contact(String name, String address, String telefon, String email) throws InvalidFormatException{
		if (!validName(name)) throw new InvalidFormatException("Cannot convert", "Invalid name");
		if (!validAddress(address)) throw new InvalidFormatException("Cannot convert", "Invalid address");
		if (!validTelefon(telefon)) throw new InvalidFormatException("Cannot convert", "Invalid phone number");
		if (!validEmail(email)) throw new InvalidFormatException("Cannot convert", "Invalid email address");
		Name = name;
		Address = address;
		Telefon = telefon;
		Email = email;
	}


	public static Contact fromString(String str, String delim) throws InvalidFormatException
	{
		String[] s = str.split(delim);
		if (s.length!=4) throw new InvalidFormatException("Cannot convert", "Invalid data");
		if (!validName(s[0])) throw new InvalidFormatException("Cannot convert", "Invalid name");
		if (!validTelefon(s[2])) throw new InvalidFormatException("Cannot convert", "Invalid phone number");
		if (!validAddress(s[1])) throw new InvalidFormatException("Cannot convert", "Invalid address");

		return new Contact(s[0], s[1], s[2], s[3]);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(Name);
		sb.append("#");
		sb.append(Address);
		sb.append("#");
		sb.append(Telefon);
		sb.append("#");
		sb.append(Email);
		sb.append("#");
		return sb.toString();
	}

	private static boolean validName(String str)
	{
		if(str!="") {
			String[] s = str.split("[\\p{Punct}\\s]+");
			if (s.length > 3) return false;
		}
		Matcher matcher = VALID_NAME_REGEX .matcher(str);
		return matcher.find();
	}

	private static boolean validAddress(String str)
	{
		return (str != null && !str.equals("") && str.length() <= 50);
	}

	private static boolean validTelefon(String tel)
	{
		Matcher matcher = VALID_PHONE_REGEX .matcher(tel);
		return matcher.find();
//
//		String[] s = tel.split("[\\p{Punct}\\s]+");
//		if (tel.charAt(0) == '+' && s.length == 2 ) return true;
//		if (tel.charAt(0) != '0')return false;
//		if (s.length != 1) return false;
//		return true;
	}

	public static boolean validEmail(String emailStr) {
		Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(emailStr);
		return matcher.find();
	}

	@Override
	public boolean equals(Object obj) {
		if (! (obj instanceof Contact)) return false;
		Contact o = (Contact)obj;
		if (Name.equals(o.Name) && Address.equals(o.Address) &&
				Telefon.equals(o.Telefon) && Email.equals(o.Email))
			return true;
		return false;
	}


	public String getName() {
		return Name;
	}

	public void setName(String name) throws InvalidFormatException {
		if (!validName(name)) throw new InvalidFormatException("Cannot convert", "Invalid name");
		Name = name;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) throws InvalidFormatException {
		if (!validAddress(address)) throw new InvalidFormatException("Cannot convert", "Invalid address");
		Address = address;
	}

	public String getTelefon() {
		return Telefon;
	}

	public void setTelefon(String telefon) throws InvalidFormatException {
		if (!validTelefon(telefon)) throw new InvalidFormatException("Cannot convert", "Invalid phone number");
		Telefon = telefon;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) throws  InvalidFormatException{
		if(!validEmail(email)) throw new InvalidFormatException("Cannot convert", "Invalid email address");
		Email = email;
	}
}
