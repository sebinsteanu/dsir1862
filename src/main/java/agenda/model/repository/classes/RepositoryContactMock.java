package agenda.model.repository.classes;

import java.util.LinkedList;
import java.util.List;

import agenda.model.base.Contact;
import agenda.model.repository.interfaces.RepositoryContact;
import agenda.exceptions.InvalidFormatException;

public class RepositoryContactMock implements RepositoryContact {

private List<Contact> contacts;
	
	public RepositoryContactMock() {
		contacts = new LinkedList<Contact>();
		try {
			Contact c = new Contact("Name Vasile", "address1", "+03112235", "email1@sd.com");
			contacts.add(c);
			c = new Contact("Name Ionel", "address 2", "07112233", "email12@sd.com");
			contacts.add(c);
			c = new Contact("Name Viorel", "address 3", "+407112233", "email3@sd.com");
			contacts.add(c);
		} catch (InvalidFormatException e) {
			System.out.printf(e.getMessage());
		}
	}

	@Override
	public List<Contact> getContacts() {
		return new LinkedList<Contact>(contacts);
	}

	@Override
	public void addContact(String name, String adress, String telefon, String email)  {
		try {
			contacts.add(new Contact(name,adress,telefon,email));
		} catch (InvalidFormatException e) {
			System.out.printf(e.getMessage());
		}
	}

	@Override
	public boolean removeContact(Contact contact) {
		int index = contacts.indexOf(contact);
		if (index < 0) return false;
		else contacts.remove(index);
		return true;
	}

	@Override
	public boolean saveContracts() {
		return true;
	}

	@Override
	public int count() {
		return contacts.size();
	}

	@Override
	public Contact getByName(String string) {
		for(Contact c : contacts)
			if (c.getName().equals(string)) return c;
		return null;
	}
	
}
